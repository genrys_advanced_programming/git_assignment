#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;


#UPDATE.....

Server::Server()
{
    // notice that we step out to the global namespace
    // for the resolution of the function socket

    // this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
    // if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
    _serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (_serverSocket == INVALID_SOCKET)
        throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
    try {
        // the only use of the destructor should be for freeing
        // resources that was allocated in the constructor
        ::closesocket(_serverSocket);
    }
    catch (...) {
    }
}

void Server::serve(int port)
{

    struct sockaddr_in sa = { 0 };

    sa.sin_port = htons(port); // port that server will listen for
    sa.sin_family = AF_INET; // must be AF_INET
    sa.sin_addr.s_addr = INADDR_ANY; // when there are few ip's for the machine. We will use always "INADDR_ANY"

    // again stepping out to the global namespace
    // Connects between the socket and the configuration (port and etc..)
    if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
        throw std::exception(__FUNCTION__ " - bind");

    // Start listening for incoming requests of clients
    if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
        throw std::exception(__FUNCTION__ " - listen");
    std::cout << "Listening on port " << port << endl;

    while (true) {
        // the main thread is only accepting clients
        // and add then to the list of handlers
        std::cout << "Waiting for client connection request" << endl;
        accept();
    }
}

void Server::accept()
{
    // this accepts the client and create a specific socket from server to this client
    SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
    if (client_socket == INVALID_SOCKET)
        throw std::exception(__FUNCTION__);

    std::cout << "Client accepted. Server and client can speak" << endl;

    // the function that handle the conversation with the client
    thread x(&Server::clientHandler, this, client_socket);
    x.detach();
}

void Server::clientHandler(SOCKET clientSocket)
{
    Helper r;

    try {
		string myUserName = "";
		int position = 0;
		int sizeBefore = 0;
        string line = "";
        int digits = 0;
        int number = 0;
        ifstream readFile;
        readFile.open("data.txt");
        string s = "101";
        string fileContent = "";
		char m[100] = { 0 };
		int x = 0;
		string tempRespond = "";
		string stringPart = "";
		int j = 0;


        while (getline(readFile, line)) {
            fileContent += line; //Takes the data from the file..
        }
        readFile.close();

        stringPart = r.getStringPartFromSocket(clientSocket, 99);
		/*
		This part is for removing special characters..
		*/
        strcpy_s(m, stringPart.c_str());
        x = stringPart.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_"); 
        if (x != std::string::npos) {
            m[x] = '\0';
        }

		//Gets the client name..
        stringPart = m;
        stringPart = stringPart.substr(5, stringPart.length());
        std::cout << "Client name is: " << stringPart << "\n";
        allSockets.insert(std::pair<string, SOCKET>(stringPart, clientSocket));
        
		myUserName = stringPart;
        position = 0;
        sizeBefore = userNames.size();

        if (std::find(userNames.begin(), userNames.end(), stringPart) == userNames.end()) {
            userNames.push_back(stringPart);
        }

        position = std::distance(userNames.begin(), std::find(userNames.begin(), userNames.end(), stringPart)) + 1;
		//^^ This line finds the index of client name in userNames..

        r.sendUpdateMessageToClient(clientSocket, fileContent, userNames.front(), (sizeBefore != 0 ? userNames[1] : ""), position);

        tempRespond = "";
        int lengthOfFile = 0;

        while (true) {
            if (userNames.front() == myUserName) {
                fstream dataFile;
                tempRespond = r.getStringPartFromSocket(clientSocket, 99);
                lengthOfFile = std::atoi(tempRespond.substr(3, 5).c_str());
                fileContent = tempRespond.substr(8, lengthOfFile);
                std::unique_lock<mutex> locker(_m);
                dataFile.open("data.txt", std::ofstream::out | std::ofstream::trunc); //Opens the file and deletes the content.
                dataFile.write(fileContent.c_str(), fileContent.length());
                dataFile.close();
                locker.unlock();
            }
            else {
                std::unique_lock<mutex> locker(_m);
                readFile.open("data.txt");
                fileContent = "";
                while (getline(readFile, line)) {
                    fileContent += line;
                }
                readFile.close();
                locker.unlock();
                _working = 1;
            }
            if (tempRespond[0] == '2' && tempRespond[1] == '0' && (tempRespond[2] == '4' || tempRespond[2] == '7') || _working == 1) {
                if (myUserName == userNames.front() && tempRespond.length() >= 2 && tempRespond[2] == '7') {
                    userNames.push_back(userNames.front());
                    userNames.erase(userNames.begin());
                    sizeBefore = userNames.size();
                    r.sendUpdateMessageToClient(allSockets[userNames.front()], fileContent, userNames.front(), (sizeBefore != 0 ? userNames[1] : ""), 1);
                }
                position = std::distance(userNames.begin(), std::find(userNames.begin(), userNames.end(), stringPart)) + 1;
                r.sendUpdateMessageToClient(clientSocket, fileContent, userNames.front(), (sizeBefore != 0 ? userNames[1] : ""), position);
                if (_working == 1) {
                    this_thread::sleep_for(5s);
                }
                _working = 0;
            }
            else if (tempRespond[0] == '2' && tempRespond[1] == '0' && tempRespond[2] == '8') {
                cout << "OOPS, " << stringPart << " disconnected..\n";
                break;
            }
        }

        closesocket(clientSocket);
        allSockets.erase(stringPart);
        for (j = 0; j < userNames.size(); j++) {
            if (userNames[j] == stringPart) {
                userNames.erase(userNames.begin() + j);
                break;
            }
        }
    }
    catch (...) {
        closesocket(clientSocket);
    }
}
