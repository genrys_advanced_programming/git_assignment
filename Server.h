#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include <fstream>
#include <thread>
#include <deque>
#include <mutex>
#include <map>

#UPDATE.....


class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);
	deque<string> userNames;
	SOCKET _serverSocket;
	map<string,SOCKET> allSockets;
	mutex _m;
	int _working;
};
